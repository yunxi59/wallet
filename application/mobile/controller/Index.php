<?php
namespace app\mobile\controller;
use think\Controller;
use think\Session;
class Index extends Controller
{
    public function _initialize(){
        header("Content-type: text/html; charset=utf-8");
        date_default_timezone_set('PRC'); //设置中国时区 
        //判断管理员是否登录
        if(!session('user_id')) {
            $this->redirect('mobile/login/login');
        }
    }
    //货币兑换，人民币
    public function huobi(){
		$url = "https://api.shenjian.io/?appid=49799ab372fc4b4e6bb8e8ad273bdda3&coin=Ethereum&currency=CNY";
		$curl = curl_init(); // 启动一个CURL会话
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HEADER, 0);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);  // 从证书中检查SSL加密算法是否存在
		$tmpInfo = curl_exec($curl);     //返回api的json对象
		//关闭URL请求
		curl_close($curl);
		return $tmpInfo;    //返回json对象
    }
    //美金的
    public function huobi_usd(){
        $url = "https://api.shenjian.io/?appid=49799ab372fc4b4e6bb8e8ad273bdda3&coin=Ethereum&currency=USD";
        $curl = curl_init(); // 启动一个CURL会话
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);  // 从证书中检查SSL加密算法是否存在
        $tmpInfo = curl_exec($curl);     //返回api的json对象
        //关闭URL请求
        curl_close($curl);
        return $tmpInfo;    //返回json对象
    }
    //首页
    public function index()
    {

        $user_id = Session::get('user_id');
        $user = db('user')->where('user_id',$user_id)->find();
        $data_huobi = db('huobi')->order('id desc')->find();//取最新的一条

        if (empty($data_huobi)) {
            $huobi = json_decode($this->huobi(),true);//人民币
            $huobi_usd = json_decode($this->huobi_usd(),true);//美金

            $add['cost'] = substr(substr($huobi['data']['cost'],2),0,-4);//人民币
            $add['usd'] = substr(substr($huobi_usd['data']['cost'],1),0,-4);//美金
            $add['trend'] = $huobi['data']['trend'];
            $add['times'] = time();
            db('huobi')->insert($add);
            $data_huobi = db('huobi')->order('id desc')->find();//取最新的一条
        }
        
        //判断一个小时更新一次
        $last_time = $data_huobi['times'] + 3600;
        $cur_time = time();
        if ($last_time < $cur_time) {
            $huobi = json_decode($this->huobi(),true);//人民币
            $huobi_usd = json_decode($this->huobi_usd(),true);//美金

            $add['cost'] = substr(substr($huobi['data']['cost'],2),0,-4);//人民币
            $add['usd'] = substr(substr($huobi_usd['data']['cost'],1),0,-4);//美金
            $add['trend'] = $huobi['data']['trend'];
            $add['times'] = time();
            db('huobi')->insert($add);
            $data_huobi = db('huobi')->order('id desc')->find();//取最新的一条
        }
        $user['user_moneys'] = $user['user_money'] * $data_huobi['cost'];
        $data_huobi['trend'] = substr($data_huobi['trend'], 0,-1);
        $this->assign('user',$user);
        $this->assign('data_huobi',$data_huobi);
        return $this->fetch();
    }
    //转入
    public function changesin()
    {
        $user_id = Session::get('user_id');
        $changesin=db('account')
        ->field('a.*,b.nickname,b.head_pic')
        ->alias('a')
        ->join('user b','a.user_id = b.user_id')
        ->where(['a.user_id'=>$user_id,'a.status'=>1])
        ->order('a.id desc')
        ->select();
        $this->assign('changesin',$changesin);
        return $this->fetch();
    }
    //转出
    public function changesout()
    {
        $user_id = Session::get('user_id');
        $changesout=db('account')
        ->field('a.*,b.nickname,b.head_pic')
        ->alias('a')
        ->join('user b','a.user_id = b.user_id')
        ->where(['a.user_id'=>$user_id,'a.status'=>2])
        ->order('a.id desc')
        ->select();
        $this->assign('changesout',$changesout);
        return $this->fetch();
        return $this->fetch();
    }
    //转账
    public function changemoney()
    {
        if(request()->isPost()){
            $user_id = Session::get('user_id');
            $user = db('user')->where('user_id',$user_id)->find();//当前用户

            $account = db('account')->where('user_id='.$user_id,'status=1 or status=0')->order('id asc')->find();
            
            if (!empty($account)) {
                $curtime = time();
                $san_date = strtotime(date("Y-m-d",strtotime("+3 month",$account['add_time'])));//三个月后
                $cha_date = ceil(($san_date - $curtime) / 86400);//剩余天数
                if ($san_date >= $account['add_time']) {
                    return json(['code'=>0,'msg'=>'三个月后才可以转出,剩余'.$cha_date.'天']);
                }
            }else{
                return json(['code'=>0,'msg'=>'金额有误,请联系管理员']);
            }
            $user_money_addr = trim(input('post.user_money_addr'));
            $user_money = trim(input('post.user_money'));
            //金额不足
            if ($user['user_money'] < $user_money) {
                return json(['code'=>0,'msg'=>'您的以太币不足']);
            }
            //不能是输入自己的钱包地址
            if ($user['user_money_addr'] == $user_money_addr) {
                return json(['code'=>0,'msg'=>'钱包地址错误']);
            }

            $other_user = db('user')->where('user_money_addr',$user_money_addr)->find();//对方账户
            $my_money = $user['user_money'] - $user_money;//扣掉自己的
            $other_money = $other_user['user_money'] + $user_money;//对方加上的金额

            $my = db('user')->where('user_id',$user_id)->setField('user_money',$my_money);
            if ($my) {
                //转出
                $my_log = [
                    'user_id'=>$user_id,
                    'user_money_addr'=>$user_money_addr,
                    'user_money'=>$user_money,
                    'add_time'=>time(),
                    'status'=>2
                ];
                db('account')->insert($my_log);

            }

            $other = db('user')->where('user_money_addr',$user_money_addr)->setField('user_money',$other_money);
            if ($other) {
                //对方接收
                $other_log = [
                    'user_id'=>$other_user['user_id'],
                    'user_money_addr'=>$user['user_money_addr'],
                    'user_money'=>$user_money,
                    'add_time'=>time(),
                    'status'=>1
                ];
                db('account')->insert($other_log);
            }
            return json(['code'=>1,'msg'=>'转账成功','url'=>url('index/index')]);
        }

        $user_id = Session::get('user_id');
        $user = db('user')->where('user_id',$user_id)->find();
        $data_huobi = db('huobi')->order('id desc')->find();//取最新的一条,huobi
        $user['user_moneys'] = $user['user_money'] * $data_huobi['cost'];
        $this->assign('user',$user);
        return $this->fetch();

        
    }

    //退出登录
    public function logout(){
        Session::delete('user_id');
        $this->redirect('mobile/login/login');
    }
    
}
