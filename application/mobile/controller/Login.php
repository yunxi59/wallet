<?php
namespace app\mobile\controller;
use think\Controller;
use think\Session;
class Login extends Controller
{
	//登录
    public function login()
    {
        if(request()->isPost()){
            $username = trim(input('post.username'));
            $password = trim(input('post.password'));
            $where = [
                'username'=>$username,
                'password'=>md5($password)
            ];
            $user = db('user')->where($where)->find();
            if (!$user) {
                return json(['code'=>0,'msg'=>'用户名错误或密码错误']);
            }
            Session::set('user_id',$user['user_id']);
            return json(['code'=>1,'msg'=>'登录成功','url'=>url('index/index')]);
        }else{
            return $this->fetch();
        }
    }
    //注册
    public function reg()
    {
        if(request()->isPost()){
            $data['username'] = trim(input('post.username'));
            $data['password'] = md5(trim(input('post.password')));
            $data['nickname'] = trim(input('post.nickname'));
            $data['add_time'] = time();
            $user = db('user')->where('username',trim(input('post.username')))->find();
            if (isset($user)) {
                return json(['code'=>0,'msg'=>'账号已存在']);
            }
            $getLastInsID = db('user')->insertGetId($data);
            if ($getLastInsID) {
                $user_money_addr = substr(strtoupper(md5($getLastInsID)),0,-16);
                db('user')->where('user_id',$getLastInsID)->setField('user_money_addr',$user_money_addr);//添加钱包地址
                return json(['code'=>1,'msg'=>'注册成功','url'=>url('login/login')]);
            }else{
                return json(['code'=>0,'msg'=>'注册失败']);
            }
        }else{
            return $this->fetch();
        }
    }
    //忘记密码
    public function forgotpassword()
    {
        return $this->fetch();
    }
}
