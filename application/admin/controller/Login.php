<?php
namespace app\admin\controller;
use think\Controller;
use think\Session;
class Login extends Controller
{
	//登录处理
    public function index()
    {
    	if(request() -> isPost()){
    		$username = trim(input('post.username'));
    		$password = trim(input('post.password'));
    		$where = [
    			'username'=>$username,
    			'password'=>md5($password)
    		]; 
    		$admin_user = db('admin')->where($where)->find();
    		if (!$admin_user) {
    			return json(['code'=>0,'msg'=>'用户名错误或密码错误']);
    		}
    		Session::set('admin_id',$admin_user['admin_id']);
    		return json(['code'=>1,'msg'=>'登录成功','url'=>url('index/index')]);
    	}else{
        	return $this->fetch();
    	}
    }
    //退出登录http://www.nongye.com/public/index.php/admin/login/index.html
    public function logout()
    {
    	Session::delete('admin_id');
    	$this->redirect('admin/login/index');
    }
}
