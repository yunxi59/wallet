<?php
namespace app\admin\controller;
use think\Controller;
use think\Session;
class User extends Controller
{
    public function member_list()
    {
        // $list = db('user')->order('user_id desc')->select();
        if (request()->isPost()) {
            $nickname = trim(input('post.nickname'));
            $where['nickname'] = ['like',"%{$nickname}%"];
        }else{
            $where = '';
        }
        $count = db('user')->where($where)->order('user_id desc')->count();
        $list = db('user')->where($where)->order('user_id desc')->paginate();
        $page = $list->render();
        $this->assign('count', $count);
        $this->assign('list',  $list);
        $this->assign('page',  $page);
        return $this->fetch();
    }
    //添加会员
    public function member_add()
    {
    	if(request()->isPost()){
            $data['username'] = trim(input('post.username'));
            $data['password'] = md5(trim(input('post.password')));
            $data['nickname'] = trim(input('post.nickname'));
            $data['add_time'] = time();
            $user = db('user')->where('username',trim(input('post.username')))->find();
            if (isset($user)) {
                return json(['code'=>0,'msg'=>'账号已经存在']);
            }
            $getLastInsID = db('user')->insertGetId($data);
            if ($getLastInsID) {
                $user_money_addr = substr(strtoupper(md5($getLastInsID)),0,-16);
                db('user')->where('user_id',$getLastInsID)->setField('user_money_addr',$user_money_addr);//添加钱包地址
                return json(['code'=>1,'msg'=>'添加成功','url'=>url('member_list')]);
            }else{
                return json(['code'=>0,'msg'=>'添加失败']);
            }
        }else{
            return $this->fetch();
        }
    }
    public function member_edit()
    {
        if(request()->isPost()){
            $data['username'] = trim(input('post.username'));
            $data['nickname'] = trim(input('post.nickname'));
            // $data['user_money'] = trim(input('post.user_money'));
            $user_id = trim(input('post.user_id'));
            $upd = db('user')->where('user_id',$user_id)->update($data);
            if ($upd) {
                return json(['code'=>1,'msg'=>'修改成功','url'=>url('member_list')]);
            }else{
                return json(['code'=>0,'msg'=>'修改失败']);
            }
        }

        $user_id = trim(input('get.user_id'));
        $user = db('user')->where('user_id',$user_id)->find();
        $this->assign('user',$user);
    	return $this->fetch();
    }
    public function member_del()
    {
        if (request()->isPost()) {
            $id = input('post.id');
            $del = db('user')->where('user_id',$id)->delete();
            if ($del) {
                return json(['code'=>1,'msg'=>'删除成功']);
            }else{
                return json(['code'=>0,'msg'=>'删除失败']);
            }
        }
    	return $this->fetch();
    }
    public function delAll(){
        $ids = implode($_POST['ids'],',');
        
        $del = db('user')->where(['user_id'=>['in',$ids]])->delete();
        if ($del) {
            return json(['code'=>1,'msg'=>'删除成功']);
        }else{
            return json(['code'=>0,'msg'=>'删除失败']);
        }

    }
    //资金调节
    public function account()
    {
        if (request()->isPost()) {
            $cate = trim(input('post.cate'));
            $user_id = trim(input('post.user_id'));
            $user_money = trim(input('post.user_money'));
            if (empty($user_money)) {
                return json(['code'=>0,'msg'=>'请填写金额']);
            }
            $user = db('user')->where('user_id',$user_id)->find();
            if ($cate == 1) {
                $moneys = $user['user_money'] + $user_money;//增加
                $log['user_money'] = $user_money;
                $log['desc'] = 'admin-增加';
            }else{
                if ($user['user_money'] < $user_money) {
                    return json(['code'=>0,'msg'=>'所剩金额不足']);
                }
                $moneys = $user['user_money'] - $user_money;//减少
                $log['user_money'] = '-'.$user_money;
                $log['desc'] = 'admin-减扣';
            }

            $upd = db('user')->where('user_id',$user_id)->setField('user_money',$moneys);
            $log['user_id'] = $user_id;
            $log['add_time'] = time();
            $add = db('account')->insert($log);
            return json(['code'=>1,'msg'=>'修改成功','url'=>url('member_list')]);
            
        }
        $this->assign('user_id',trim(input('get.user_id')));
    	return $this->fetch();
    }
    //资金记录
    public function accountlog(){
        $log=db('account')
        ->field('a.*,b.nickname,b.head_pic')
        ->alias('a')
        ->join('user b','a.user_id = b.user_id')
        ->order('a.id desc')
        ->paginate();
        
        $count = db('account')->order('user_id desc')->count();
        $page = $log->render();
        $this->assign('count', $count);
        $this->assign('page',  $page);
        $this->assign('list',$log);
        // ->where(['a.user_id'=>$user_id,'a.status'=>2])
        return $this->fetch();
    }
}
