<?php
namespace app\admin\controller;
use think\Request;
use think\Session;
class Index extends Base
{
    public function index()
    {
        return $this->fetch();
    }
    public function welcome()
    {
    	$admin_id = Session::get('admin_id');
    	$admin = db('admin')->where('admin_id',$admin_id)->find();
    	$count_user = db('user')->count();
    	$this->assign('admin_user',$admin['username']);
    	$this->assign('curr_time',time());
    	$this->assign('count_user',$count_user);
    	return $this->fetch();
    }
}
