<?php
namespace app\admin\controller;
use think\Controller;
class Base extends Controller {

    public function _initialize(){
        date_default_timezone_set('PRC'); //设置中国时区 
        //判断管理员是否登录
        if(!session('admin_id')) {
            $this->redirect('admin/login/index');
        }
    }
}