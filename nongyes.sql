/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : nongyes

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2018-08-18 21:23:54
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tp_account
-- ----------------------------
DROP TABLE IF EXISTS `tp_account`;
CREATE TABLE `tp_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '这是转账的那个用户',
  `user_money_addr` varchar(64) NOT NULL COMMENT '对方接收的转账地址',
  `user_money` decimal(10,3) NOT NULL,
  `add_time` varchar(16) NOT NULL,
  `status` int(2) NOT NULL COMMENT '1转入 2转出',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tp_account
-- ----------------------------
INSERT INTO `tp_account` VALUES ('2', '26', '02E74F10E0327AD868D138F2B4FDD6F0', '5.000', '1534591186', '2');
INSERT INTO `tp_account` VALUES ('3', '27', '4E732CED3463D06DE0CA9A15B6153677', '5.000', '1534591186', '1');
INSERT INTO `tp_account` VALUES ('4', '26', '02E74F10E0327AD868D138F2B4FDD6F0', '5.000', '1534591208', '2');
INSERT INTO `tp_account` VALUES ('5', '27', '4E732CED3463D06DE0CA9A15B6153677', '5.000', '1534591208', '1');
INSERT INTO `tp_account` VALUES ('6', '27', '4E732CED3463D06DE0CA9A15B6153677', '5.000', '1534591590', '2');
INSERT INTO `tp_account` VALUES ('7', '26', '02E74F10E0327AD868D138F2B4FDD6F0', '5.000', '1534591590', '1');
INSERT INTO `tp_account` VALUES ('8', '26', '02E74F10E0327AD868D138F2B4FDD6F0', '10.000', '1534593005', '2');
INSERT INTO `tp_account` VALUES ('9', '27', '4E732CED3463D06DE0CA9A15B6153677', '10.000', '1534593005', '1');
INSERT INTO `tp_account` VALUES ('10', '26', '02E74F10E0327AD868D138F2B4FDD6F0', '10.000', '1534593135', '2');
INSERT INTO `tp_account` VALUES ('11', '27', '4E732CED3463D06DE0CA9A15B6153677', '10.000', '1534593135', '1');

-- ----------------------------
-- Table structure for tp_admin
-- ----------------------------
DROP TABLE IF EXISTS `tp_admin`;
CREATE TABLE `tp_admin` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL,
  `add_time` varchar(32) NOT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tp_admin
-- ----------------------------
INSERT INTO `tp_admin` VALUES ('1', 'admin', '21232f297a57a5a743894a0e4a801fc3', '');

-- ----------------------------
-- Table structure for tp_huobi
-- ----------------------------
DROP TABLE IF EXISTS `tp_huobi`;
CREATE TABLE `tp_huobi` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `usd` decimal(10,2) NOT NULL,
  `cost` decimal(10,2) NOT NULL,
  `trend` varchar(16) NOT NULL,
  `times` varchar(16) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tp_huobi
-- ----------------------------
INSERT INTO `tp_huobi` VALUES ('6', '307.04', '2112.27', '2.57%', '1534598035');

-- ----------------------------
-- Table structure for tp_user
-- ----------------------------
DROP TABLE IF EXISTS `tp_user`;
CREATE TABLE `tp_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(64) NOT NULL DEFAULT '',
  `password` varchar(64) NOT NULL,
  `nickname` varchar(64) NOT NULL,
  `head_pic` varchar(128) NOT NULL,
  `user_money` decimal(10,3) NOT NULL,
  `user_money_addr` varchar(64) NOT NULL,
  `add_time` varchar(16) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tp_user
-- ----------------------------
INSERT INTO `tp_user` VALUES ('26', '18225347029', '4e01fb2f582080b195ecf79ae491182a', '18225347029', '', '75.000', '4E732CED3463D06DE0CA9A15B6153677', '1534583034');
INSERT INTO `tp_user` VALUES ('27', '15923092174', '02ebfb5cc47b9635085361b27daea71f', '15923092174', '', '125.000', '02E74F10E0327AD868D138F2B4FDD6F0', '1534587336');
